# Calculate the price of an orderSummary

Given a CSV file with the following lines:
```
<ProductID>,<Stock>,<Price>
```

Write a command line program that calculates the total price of an orderSummary given the following rules:
* The total amount to be paid is the sum of the price of each product in the orderSummary multiplied by the quantity of each item in the orderSummary
* Prices on the CSV file do not include VAT
* The total amount to be paid must include VAT at the fixed rate of 23%
* If a product is out of stock, the program must end with error code 1 and display a message

## Command line interface
The program must run from the command line with the following program arguments:

* `<Path_to_orders_file>`: path to the file to be processed

e.g. `dotnet run OrderPrice.dll orders.csv` or `java -jar OrderPrice.jar orders.csv`

## Example
Given the input `Catalog.txt` file
```
P4,10,250.00
P10,5,175.00
P12,5,1000.00
```

```
$ CalculateOrder Catalog.txt P4 6 P10 5 P12 1
Total: 4151,25
```

## Deliverable
We expect you to deliver a zip file containing the source code that implements the solution for this problem.
Please provide clear instructions on how to build and run the application.

# Tips
We are expecting a simple solution to this problem. No need to over-engineer it nor introduce unnecessary complexity.

Remember to keep the code elegant and efficient.