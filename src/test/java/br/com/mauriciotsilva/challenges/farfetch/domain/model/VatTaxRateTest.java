package br.com.mauriciotsilva.challenges.farfetch.domain.model;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.VatTaxRate;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static java.math.BigDecimal.TEN;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class VatTaxRateTest {

    private static final int PERCENTAGE = 20;
    private static final BigDecimal TOTAL_PRICE = TEN;
    private static final BigDecimal TOTAL_PRICE_WITH_VAT = BigDecimal.valueOf(12.0);

    private VatTaxRate subject;

    @Before
    public void setup() {
        subject = new VatTaxRate(PERCENTAGE);
    }

    @Test
    public void testApply() {
        var value = subject.apply(TOTAL_PRICE);
        assertThat(value, equalTo(TOTAL_PRICE_WITH_VAT));
    }
}