package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.Order.Item;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CheckAvailableStockSpecTest {

    private static final Integer QUANTITY = 10;

    @Mock
    private StockItems stockItems;
    private CheckAvailableStockSpec subject;

    @Mock
    private StockItem stockItem;
    @Mock
    private Product.ProductId productId;
    @Mock
    private Item item;


    @Before
    public void setup() {

        when(item.getQuantity()).thenReturn(QUANTITY);
        when(item.getProductId()).thenReturn(productId);
        when(stockItem.isQuantityAvailable(QUANTITY)).thenReturn(true);
        when(stockItems.byProductId(productId)).thenReturn(ofNullable(stockItem));

        subject = new CheckAvailableStockSpec(stockItems);
    }

    @Test
    public void testAvailable() {

        var result = subject.test(item);
        assertThat(result, is(true));
    }

    @Test
    public void testUnavailableWhenQuantityUnavailable() {

        when(stockItem.isQuantityAvailable(QUANTITY)).thenReturn(false);

        var result = subject.test(item);
        assertThat(result, is(false));
    }

    @Test
    public void testUnavailableWhenProductIdNotFound() {

        when(stockItems.byProductId(productId)).thenReturn(empty());

        var result = subject.test(item);
        assertThat(result, is(false));
    }
}