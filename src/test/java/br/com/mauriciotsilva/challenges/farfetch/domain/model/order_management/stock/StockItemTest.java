package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StockItemTest {

    private static final int TOTAL_QUANTITY_AVAILABLE = 3;

    @Mock
    private Product.ProductId productId;

    private StockItem subject;

    @Before
    public void setup() {
        subject = new StockItem(productId, TOTAL_QUANTITY_AVAILABLE);
    }

    @Test
    public void testAvailableQuantity() {
        assertThat(subject.isQuantityAvailable(TOTAL_QUANTITY_AVAILABLE), is(true));
    }

    @Test
    public void testUnavailableQuantity() {
        var unavailable = TOTAL_QUANTITY_AVAILABLE + 1;
        assertThat(subject.isQuantityAvailable(unavailable), is(false));
    }
}