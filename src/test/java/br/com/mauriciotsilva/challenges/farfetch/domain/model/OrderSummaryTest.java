package br.com.mauriciotsilva.challenges.farfetch.domain.model;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.Order.Item;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.VatTaxRate;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.CheckAvailableStockSpec;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static java.math.BigDecimal.TEN;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderSummaryTest {

    private Order subject;

    @Mock
    private CheckAvailableStockSpec availableStockSpec;
    @Mock
    private Item item;
    @Mock
    private VatTaxRate vatTaxRate;

    @Before
    public void setup() {

        when(item.getQuantity()).thenReturn(1);
        when(item.getPrice()).thenReturn(BigDecimal.valueOf(5));
        when(vatTaxRate.apply(any())).thenAnswer(invocation -> invocation.getArgument(0));

        when(availableStockSpec.test(item)).thenReturn(true);
        subject = new Order();
    }

    @Test
    public void testCalculateTotalPrice() {

        var anotherItem = mock(Item.class);
        when(anotherItem.getPrice()).thenReturn(BigDecimal.valueOf(5));
        when(anotherItem.getQuantity()).thenReturn(1);
        when(availableStockSpec.test(anotherItem)).thenReturn(true);

        subject.addItem(item, availableStockSpec);
        subject.addItem(anotherItem, availableStockSpec);

        var totalPrice = subject.calculateTotalPrice(vatTaxRate);

        verify(vatTaxRate).apply(TEN);
        assertThat(totalPrice, is(TEN));
    }

    @Test
    public void testAddItem() {

        subject.addItem(item, availableStockSpec);
        assertThat(subject.getItems(), containsInAnyOrder(item));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddItemWhenAlreadyExists() {

        subject.addItem(item, availableStockSpec);
        subject.addItem(item, availableStockSpec);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddItemWhenUnavailableStock() {

        when(availableStockSpec.test(item)).thenReturn(false);
        subject.addItem(item, availableStockSpec);
    }

    @Test
    public void testItemEquals() {

        var quantity = 0;
        var price = TEN;
        var productId = mock(Product.ProductId.class);

        var anItem = new Item(productId, quantity, price);
        var theSameItem = new Item(productId, quantity, price);
        var anotherItem = new Item(mock(Product.ProductId.class), quantity, price);

        assertThat("should be equals when productId is the same", anItem.equals(anItem), is(true));
        assertThat("should be equals when the some object", anItem.equals(theSameItem), is(true));
        assertThat("should not be equals when productId is diff", anItem.equals(anotherItem), is(false));
    }
}