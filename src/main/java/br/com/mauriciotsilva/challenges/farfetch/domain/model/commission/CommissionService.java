package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.function.Predicate;

import static java.math.BigDecimal.ZERO;
import static java.util.function.Predicate.not;

@Service
public class CommissionService {

    private final Orders orders;
    private final CommissionTaxRate taxRate;

    @Autowired
    public CommissionService(Orders orders, CommissionTaxRate taxRate) {
        this.orders = orders;
        this.taxRate = taxRate;
    }

    public BigDecimal calculateTotalTaxRate(Boutique boutique) {

        var totalPrice = boutique.getOrders().stream()
                .filter(not(new HighestOrderOfAllSpecification()))
                .map(order -> taxRate.apply(order.getTotalPrice()))
                .reduce(ZERO, (price, anotherPrice) -> price.add(anotherPrice));

        return totalPrice;
    }

    private class HighestOrderOfAllSpecification implements Predicate<OrderSummary> {

        private final OrderSummary highest;

        private HighestOrderOfAllSpecification() {
            this.highest = orders.highest().orElse(null);
        }

        @Override
        public boolean test(OrderSummary orderSummary) {
            return highest != null && highest.equals(orderSummary);
        }
    }
}