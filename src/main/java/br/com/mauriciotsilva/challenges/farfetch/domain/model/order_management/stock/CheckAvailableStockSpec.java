package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Order.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;


@Component
public class CheckAvailableStockSpec implements Predicate<Item> {

    private final StockItems stockItems;

    @Autowired
    public CheckAvailableStockSpec(StockItems stockItems) {
        this.stockItems = stockItems;
    }

    public boolean test(Item item) {
        return stockItems
                .byProductId(item.getProductId())
                .filter(stockItem -> stockItem.isQuantityAvailable(item.getQuantity()))
                .isPresent();
    }
}
