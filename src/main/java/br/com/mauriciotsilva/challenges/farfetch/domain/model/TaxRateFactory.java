package br.com.mauriciotsilva.challenges.farfetch.domain.model;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.CommissionTaxRate;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.VatTaxRate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TaxRateFactory {

    @Bean
    public CommissionTaxRate createCommission() {
        return new CommissionTaxRate(10);
    }

    @Bean
    public VatTaxRate createVat() {
        return new VatTaxRate(23);
    }
}
