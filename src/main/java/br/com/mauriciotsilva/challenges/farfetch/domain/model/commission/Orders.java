package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import java.util.Optional;

public interface Orders {

    void add(OrderSummary orderSummary);

    Optional<OrderSummary> highest();
}
