package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.LisbonOperation;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;

@LisbonOperation(alias = {"CalculateOrder", "calculateOrder"})
public class CalculateOrderOperation implements Operation {

    private final Logger logger = LoggerFactory.getLogger(CalculateOrderOperation.class);
    private final CalculateOrderOperationFacade facade;

    @Autowired
    public CalculateOrderOperation(CalculateOrderOperationFacade facade) {
        this.facade = facade;
    }

    public void validate(String... params) {

        if (params.length % 2 == 0) {
            throw new IllegalArgumentException("invalid params: " + Arrays.toString(params));
        }

        var path = Path.of(params[0]);
        if (!Files.isReadable(path)) {
            throw new IllegalArgumentException("It's not possible read file: " + path);
        }
    }

    public void onExecute(String... params) throws IOException {

        var file = Path.of(params[0]);
        var entries = new HashMap<Product.ProductId, Integer>();

        for (var i = 1; i < params.length; i++) {

            var key = new Product.ProductId(params[i]);
            var value = Integer.valueOf(params[++i]);

            var result = entries.compute(key, (k, v) -> v == null ? value : v + value);
            logger.trace("Generated entry '{}={}'", key, result);
        }

        facade.execute(file, entries);
    }
}
