package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations;

import br.com.mauriciotsilva.challenges.farfetch.application.order_management.CatalogApplicationService;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.Translator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CsvCatalogLineToSyncSolicitation implements Translator<String, CatalogApplicationService.CatalogSyncSolicitation> {

    private static final String FIELD_DELIMITER = ",";

    private final Logger logger = LoggerFactory.getLogger(CsvCatalogLineToSyncSolicitation.class);

    public CatalogApplicationService.CatalogSyncSolicitation translate(String line) {

        var values = line.split(FIELD_DELIMITER);

        var productId = new Product.ProductId(values[0]);
        var price = new BigDecimal(values[2]);
        var quantity = Integer.valueOf(values[1]);

        return new CatalogApplicationService.CatalogSyncSolicitation(productId, price, quantity);
    }
}
