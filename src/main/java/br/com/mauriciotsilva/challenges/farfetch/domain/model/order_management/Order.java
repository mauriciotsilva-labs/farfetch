package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.CheckAvailableStockSpec;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static java.math.BigDecimal.ZERO;
import static java.util.Collections.unmodifiableSet;

public class Order {

    private final Set<Item> items = new HashSet<>();

    public BigDecimal calculateTotalPrice(VatTaxRate vatTaxRate) {
        var totalPrice = items.stream()
                .map(item -> item.getPrice().multiply(new BigDecimal(item.getQuantity())))
                .reduce(ZERO, (price, toAdd) -> price.add(toAdd));

        return vatTaxRate.apply(totalPrice);
    }

    public void addItem(Item item, CheckAvailableStockSpec availableStockSpec) {

        if (items.contains(item)) {
            throw new IllegalArgumentException("Product " + item.getProductId() + " already added to order");
        }

        if (!availableStockSpec.test(item)) {
            throw new IllegalArgumentException("No stock for product " + item.getProductId());
        }

        items.add(item);
    }

    public Set<Item> getItems() {
        return unmodifiableSet(items);
    }

    public static class Item {

        private final Product.ProductId productId;
        private final Integer quantity;
        private final BigDecimal price;

        public Item(Product.ProductId productId, Integer quantity, BigDecimal price) {
            this.productId = productId;
            this.quantity = quantity;
            this.price = price;
        }

        public Product.ProductId getProductId() {
            return productId;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public Integer getQuantity() {
            return quantity;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Item item = (Item) o;
            return productId.equals(item.productId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(productId);
        }
    }
}
