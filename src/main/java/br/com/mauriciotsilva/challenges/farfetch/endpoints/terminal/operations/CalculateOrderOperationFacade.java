package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations;

import br.com.mauriciotsilva.challenges.farfetch.application.order_management.CalculateOrderSolicitation;
import br.com.mauriciotsilva.challenges.farfetch.application.order_management.CalculateOrderSolicitation.ItemToCalculate;
import br.com.mauriciotsilva.challenges.farfetch.application.order_management.CatalogApplicationService;
import br.com.mauriciotsilva.challenges.farfetch.application.order_management.OrderApplicationService;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.io.CatalogSyncSolicitationCsvFileReaderAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

import static java.util.stream.Collectors.toSet;

@Component
public class CalculateOrderOperationFacade {

    private final Logger logger = LoggerFactory.getLogger(CalculateOrderOperationFacade.class);

    private final CatalogApplicationService catalogApplicationService;
    private final OrderApplicationService orderApplicationService;
    private final CatalogSyncSolicitationCsvFileReaderAdapter catalogSyncSolicitationCsvFileReaderAdapter;

    @Autowired
    public CalculateOrderOperationFacade(
            CatalogApplicationService catalogApplicationService, OrderApplicationService orderApplicationService,
            CatalogSyncSolicitationCsvFileReaderAdapter catalogSyncSolicitationCsvFileReaderAdapter) {
        this.catalogApplicationService = catalogApplicationService;
        this.orderApplicationService = orderApplicationService;
        this.catalogSyncSolicitationCsvFileReaderAdapter = catalogSyncSolicitationCsvFileReaderAdapter;
    }

    public void execute(Path path, Map<Product.ProductId, Integer> entries) throws IOException {

        var items = entries.entrySet().stream()
                .map(entry -> new ItemToCalculate(entry.getKey(), entry.getValue()))
                .collect(toSet());

        catalogApplicationService.sync(catalogSyncSolicitationCsvFileReaderAdapter.adapt(path));
        orderApplicationService.calculate(new CalculateOrderSolicitation(items), price -> logger.info("Total: {}", price));
    }
}
