package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import java.util.Set;
import java.util.TreeSet;

import static java.util.Collections.unmodifiableSet;

public class Boutique {

    private final BoutiqueId id;
    private final TreeSet<OrderSummary> orders = new TreeSet<>(new HighestOrderSummaryByTotalPrice());

    public Boutique(BoutiqueId id, Set<OrderSummary> orders) {
        this.id = id;
        this.orders.addAll(orders);
    }

    public BoutiqueId getId() {
        return id;
    }

    public OrderSummary highestOrder() {
        return orders.first();
    }

    public Set<OrderSummary> getOrders() {
        return unmodifiableSet(orders);
    }

    public static class BoutiqueId {

        private final String value;

        public BoutiqueId(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }
    }
}
