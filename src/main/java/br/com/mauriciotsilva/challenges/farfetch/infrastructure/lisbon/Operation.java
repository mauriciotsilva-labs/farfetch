package br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon;

import java.io.IOException;

public interface Operation {

    void onExecute(String... params) throws IOException;

    default void validate(String... params) {
    }

    default void execute(String... params) throws IOException {
        validate(params);
        onExecute(params);
    }
}
