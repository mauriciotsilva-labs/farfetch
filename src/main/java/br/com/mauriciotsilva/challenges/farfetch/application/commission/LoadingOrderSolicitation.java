package br.com.mauriciotsilva.challenges.farfetch.application.commission;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique.BoutiqueId;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.OrderSummary.OrderId;

import java.math.BigDecimal;

public class LoadingOrderSolicitation {

    private final BoutiqueId boutiqueId;
    private final OrderId orderId;
    private final BigDecimal price;

    public LoadingOrderSolicitation(BoutiqueId boutiqueId, OrderId orderId, BigDecimal price) {
        this.boutiqueId = boutiqueId;
        this.orderId = orderId;
        this.price = price;
    }

    public BoutiqueId getBoutiqueId() {
        return boutiqueId;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public OrderId getOrderId() {
        return orderId;
    }
}
