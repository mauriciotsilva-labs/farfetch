package br.com.mauriciotsilva.challenges.farfetch.application.order_management;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Order;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Order.Item;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Products;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.VatTaxRate;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.CheckAvailableStockSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.function.Consumer;

@Service
public class OrderApplicationService {

    private final Products products;
    private final CheckAvailableStockSpec checkAvailableStockSpec;
    private final VatTaxRate vatTaxRate;

    @Autowired
    public OrderApplicationService(
            Products products, CheckAvailableStockSpec checkAvailableStockSpec, VatTaxRate vatTaxRate) {
        this.products = products;
        this.checkAvailableStockSpec = checkAvailableStockSpec;
        this.vatTaxRate = vatTaxRate;
    }

    public void calculate(CalculateOrderSolicitation solicitation, Consumer<BigDecimal> consumer) {

        var order = new Order();
        solicitation.getItems().stream()
                .map(itemToCalculate -> toItem(itemToCalculate))
                .forEach(item -> order.addItem(item, checkAvailableStockSpec));

        consumer.accept(order.calculateTotalPrice(vatTaxRate));
    }

    private Item toItem(CalculateOrderSolicitation.ItemToCalculate item) {
        return products.byId(item.getProductId())
                .map(product -> new Item(product.getId(), item.getQuantity(), product.getPrice()))
                .orElseThrow(() -> new IllegalArgumentException("Product " + item.getProductId() + " not found"));
    }
}
