package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management;

import java.util.Optional;

public interface Products {

    Product add(Product product);

    Optional<Product> byId(Product.ProductId id);
}
