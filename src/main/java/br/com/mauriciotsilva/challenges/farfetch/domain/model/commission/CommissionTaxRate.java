package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.TaxRate;

import java.math.BigDecimal;

public class CommissionTaxRate implements TaxRate {

    private final int value;

    public CommissionTaxRate(int value) {
        this.value = value;
    }

    @Override
    public BigDecimal apply(BigDecimal price) {
        return price;
    }
}
