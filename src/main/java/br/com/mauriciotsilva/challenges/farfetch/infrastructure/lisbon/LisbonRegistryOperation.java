package br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

public class LisbonRegistryOperation {

    private final Map<String, Operation> operations = new HashMap<>();

    public LisbonRegistryOperation add(String name, Operation operation) {
        operations.put(name, operation);
        return this;
    }

    public Optional<Operation> lookup(String name) {
        return ofNullable(operations.get(name));
    }
}
