package br.com.mauriciotsilva.challenges.farfetch.infrastructure;

import java.util.Collection;
import java.util.stream.Collector;
import java.util.stream.Stream;

public interface Translator<I, O> {

    O translate(I input);

    default <R extends Collection<O>> R each(Stream<I> items, Collector<O, ?, R> collector) {
        return items.map(this::translate).collect(collector);
    }
}
