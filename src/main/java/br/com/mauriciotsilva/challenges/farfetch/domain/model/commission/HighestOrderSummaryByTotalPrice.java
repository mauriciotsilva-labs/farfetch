package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import java.util.Comparator;

public class HighestOrderSummaryByTotalPrice implements Comparator<OrderSummary> {

    @Override
    public int compare(OrderSummary orderSummary, OrderSummary anotherOrder) {
        return anotherOrder.getTotalPrice().compareTo(orderSummary.getTotalPrice());
    }
}