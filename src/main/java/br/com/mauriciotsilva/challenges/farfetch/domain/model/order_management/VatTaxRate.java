package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.TaxRate;

import java.math.BigDecimal;

public class VatTaxRate implements TaxRate {

    private static final BigDecimal DIVISOR = BigDecimal.valueOf(100);

    private final int value;

    public VatTaxRate(int value) {
        this.value = value;
    }

    public BigDecimal apply(BigDecimal price) {
        return price.multiply(toPercentage()).add(price);
    }

    private BigDecimal toPercentage() {
        return BigDecimal.valueOf(value).divide(DIVISOR);
    }
}
