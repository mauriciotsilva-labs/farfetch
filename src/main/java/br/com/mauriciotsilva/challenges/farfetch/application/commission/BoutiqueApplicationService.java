package br.com.mauriciotsilva.challenges.farfetch.application.commission;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.*;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique.BoutiqueId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;
import java.util.function.Consumer;

import static java.util.stream.Collectors.*;

@Service
public class BoutiqueApplicationService {

    private final Boutiques boutiques;
    private final CommissionService commissionService;
    private final Orders orders;

    @Autowired
    public BoutiqueApplicationService(
            Boutiques boutiques, CommissionService commissionService, Orders orders) {
        this.boutiques = boutiques;
        this.commissionService = commissionService;
        this.orders = orders;
    }

    public void loadOrders(Set<LoadingOrderSolicitation> solicitations) {
        var entries = solicitations.stream()
                .collect(groupingBy(LoadingOrderSolicitation::getBoutiqueId))
                .entrySet();

        var boutiques = entries.stream().map(entry -> {
            var orders = entry.getValue().stream()
                    .map(solicitation -> new OrderSummary(solicitation.getOrderId(), solicitation.getPrice()))
                    .collect(toSet());

            return new Boutique(entry.getKey(), orders);
        }).collect(toList());

        boutiques.forEach(boutique -> {
            this.boutiques.add(boutique);
            boutique.getOrders().forEach(orders::add);
        });

    }

    public void calculateTotalCommission(BoutiqueId boutiqueId, Consumer<BigDecimal> consumer) {

        var boutique = boutiques.byId(boutiqueId)
                .orElseThrow();

        consumer.accept(commissionService.calculateTotalTaxRate(boutique));
    }
}
