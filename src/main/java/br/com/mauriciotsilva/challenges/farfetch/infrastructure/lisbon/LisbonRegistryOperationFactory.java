package br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Map.Entry;

import static java.util.Arrays.stream;
import static java.util.Optional.ofNullable;

@Component
public class LisbonRegistryOperationFactory {

    private final Map<String, Operation> operations;

    @Autowired
    public LisbonRegistryOperationFactory(Map<String, Operation> operations) {
        this.operations = operations;
    }

    public LisbonRegistryOperation create() {

        var registry = new LisbonRegistryOperation();
        operations.entrySet().stream().forEach(entry -> registerAlias(registry, entry));

        return registry;
    }

    private void registerAlias(LisbonRegistryOperation registry, Entry<String, Operation> entry) {

        var operation = entry.getValue();

        ofNullable(operation.getClass().getAnnotation(LisbonOperation.class))
                .ifPresent(annotation -> stream(annotation.alias()).forEach(alias -> registry.add(alias, operation)));
    }
}
