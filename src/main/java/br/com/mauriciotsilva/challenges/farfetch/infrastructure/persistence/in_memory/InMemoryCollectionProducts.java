package br.com.mauriciotsilva.challenges.farfetch.infrastructure.persistence.in_memory;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Products;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Repository
public class InMemoryCollectionProducts implements Products {


    private final Map<Product.ProductId, Product> products = new HashMap<>();

    public Product add(Product product) {
        products.put(product.getId(), product);
        return product;
    }

    public Optional<Product> byId(Product.ProductId id) {
        return ofNullable(products.get(id));
    }
}
