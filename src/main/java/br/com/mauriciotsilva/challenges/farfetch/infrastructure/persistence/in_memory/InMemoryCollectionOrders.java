package br.com.mauriciotsilva.challenges.farfetch.infrastructure.persistence.in_memory;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.HighestOrderSummaryByTotalPrice;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.OrderSummary;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.OrderSummary.OrderId;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Orders;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class InMemoryCollectionOrders implements Orders {

    private final Map<OrderId, OrderSummary> orders = new HashMap<>();

    @Override
    public void add(OrderSummary orderSummary) {
        orders.put(orderSummary.getId(), orderSummary);
    }

    @Override
    public Optional<OrderSummary> highest() {
        return orders.values().stream()
                .max(new HighestOrderSummaryByTotalPrice());
    }
}
