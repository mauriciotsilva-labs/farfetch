package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import java.math.BigDecimal;
import java.util.Objects;

public class OrderSummary {

    private final OrderId id;
    private final BigDecimal totalPrice;

    public OrderSummary(OrderId id, BigDecimal totalPrice) {
        this.id = id;
        this.totalPrice = totalPrice;
    }

    public OrderId getId() {
        return id;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public static class OrderId {

        private final String value;

        public OrderId(String value) {
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            OrderId orderId = (OrderId) o;
            return value.equals(orderId.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderSummary orderSummary = (OrderSummary) o;
        return id.equals(orderSummary.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
