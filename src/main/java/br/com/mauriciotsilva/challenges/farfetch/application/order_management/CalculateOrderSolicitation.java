package br.com.mauriciotsilva.challenges.farfetch.application.order_management;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product.ProductId;

import java.util.Set;

public class CalculateOrderSolicitation {

    private final Set<ItemToCalculate> items;

    public CalculateOrderSolicitation(Set<ItemToCalculate> items) {
        this.items = items;
    }

    public Set<ItemToCalculate> getItems() {
        return items;
    }

    public static class ItemToCalculate {

        private final Integer quantity;
        private final ProductId productId;

        public ItemToCalculate(ProductId productId, Integer quantity) {
            this.productId = productId;
            this.quantity = quantity;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public ProductId getProductId() {
            return productId;
        }

        @Override
        public String toString() {
            return String.join("=", productId.toString(), quantity.toString());
        }
    }
}
