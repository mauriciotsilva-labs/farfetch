package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations;

import br.com.mauriciotsilva.challenges.farfetch.application.commission.BoutiqueApplicationService;
import br.com.mauriciotsilva.challenges.farfetch.application.commission.LoadingOrderSolicitation;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique.BoutiqueId;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.OrderSummary.OrderId;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.LisbonOperation;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.Operation;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.util.stream.Collectors.toSet;

@LisbonOperation(alias = "orderBatch")
public class OrderBatchOperation implements Operation {

    private final BoutiqueApplicationService boutiqueApplicationService;

    public OrderBatchOperation(BoutiqueApplicationService boutiqueApplicationService) {
        this.boutiqueApplicationService = boutiqueApplicationService;
    }

    @Override
    public void onExecute(String... params) throws IOException {
        var solicitations = Files.readAllLines(Path.of(params[0])).stream().map(line -> line.split(","))
                .map(param -> {
                    var boutiqueId = new BoutiqueId(param[0]);
                    var orderId = new OrderId(param[1]);
                    var price = new BigDecimal(param[2]);

                    return new LoadingOrderSolicitation(boutiqueId, orderId, price);
                }).collect(toSet());

        boutiqueApplicationService.loadOrders(solicitations);
        solicitations
                .forEach(
                        solicitation -> boutiqueApplicationService
                                .calculateTotalCommission(solicitation.getBoutiqueId(), price -> System.out.println(solicitation.getBoutiqueId() + "," + price)));
    }
}
