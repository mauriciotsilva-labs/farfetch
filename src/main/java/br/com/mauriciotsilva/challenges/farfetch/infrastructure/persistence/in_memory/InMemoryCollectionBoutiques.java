package br.com.mauriciotsilva.challenges.farfetch.infrastructure.persistence.in_memory;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique.BoutiqueId;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutiques;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Repository
public class InMemoryCollectionBoutiques implements Boutiques {

    private final Map<BoutiqueId, Boutique> boutiques = new HashMap<>();

    @Override
    public void add(Boutique boutique) {
        boutiques.put(boutique.getId(), boutique);
    }

    @Override
    public Optional<Boutique> byId(BoutiqueId id) {
        return ofNullable(boutiques.get(id));
    }
}
