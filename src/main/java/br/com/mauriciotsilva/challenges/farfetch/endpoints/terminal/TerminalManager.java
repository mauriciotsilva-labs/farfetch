package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal;

import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.LisbonRegistryOperation;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.LisbonRegistryOperationFactory;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

import static java.util.Arrays.copyOfRange;
import static java.util.Optional.ofNullable;

@Component
public class TerminalManager implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(TerminalManager.class);

    private final LisbonRegistryOperationFactory lisbonRegistryOperationFactory;

    @Autowired
    public TerminalManager(
            LisbonRegistryOperationFactory lisbonRegistryOperationFactory) {
        this.lisbonRegistryOperationFactory = lisbonRegistryOperationFactory;
    }

    @Override
    public void run(String... args) {

        var registry = lisbonRegistryOperationFactory.create();

        try (Scanner scanner = new Scanner(System.in)) {
            while (true) {

                System.out.print("operation:$ ");

                var lineParams = ofNullable(scanner.nextLine())
                        .filter(line -> !line.isBlank())
                        .map(line -> line.split("\\s"));

                lineParams.ifPresent(params -> {
                    var slice = copyOfRange(params, 1, params.length);
                    doIt(registry, params[0], slice);
                });
            }
        }
    }

    private void doIt(LisbonRegistryOperation registry, String operation, String... params) {
        try {

            Operation fault = args -> logger.warn("Operation '{}' not found", operation);
            registry.lookup(operation).orElse(fault).execute(params);
        } catch (Exception e) {
            logger.error("Error on executing operation '{}': {}", operation, e.getMessage(), e);
        }
    }
}
