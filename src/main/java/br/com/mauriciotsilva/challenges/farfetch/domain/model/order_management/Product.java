package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management;

import java.math.BigDecimal;
import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class Product {

    private final ProductId id;
    private BigDecimal price;

    public Product(ProductId id, BigDecimal price) {
        this.id = id;
        setPrice(price);
    }

    public ProductId getId() {
        return id;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public static class ProductId {

        private final String value;

        public ProductId(String value) {
            this.value = requireNonNull(value, "Should have a value for product id");
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ProductId productId = (ProductId) o;
            return value.equals(productId.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(value);
        }

        @Override
        public String toString() {
            return value;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id.equals(product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return String.join(":", id.toString(), price.toString());
    }
}

