package br.com.mauriciotsilva.challenges.farfetch.application.order_management;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.StockItems;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Products;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CatalogApplicationService {

    private final Logger logger = LoggerFactory.getLogger(CatalogApplicationService.class);

    private final Products products;
    private final StockItems stockItems;

    @Autowired
    public CatalogApplicationService(Products products, StockItems stockItems) {
        this.products = products;
        this.stockItems = stockItems;
    }

    public void sync(List<CatalogSyncSolicitation> solicitations) {

        logger.info("Bulk sync started");
        solicitations.forEach(this::sync);
        logger.info("Bulk sync finished");
    }

    public void sync(CatalogSyncSolicitation solicitation) {

        logger.trace(
                "Importing product '{}' with price '{}' with '{}' quantities",
                solicitation.getProductId(),
                solicitation.getPrice(),
                solicitation.getQuantity()
        );

        var product = products
                .byId(solicitation.getProductId())
                .orElseGet(() -> products.add(new Product(solicitation.getProductId(), solicitation.getPrice())));

        product.setPrice(solicitation.getPrice());
        stockItems.update(product, solicitation.getQuantity());
    }

    public static class CatalogSyncSolicitation {

        private final Product.ProductId productId;
        private final BigDecimal price;
        private final Integer quantity;

        public CatalogSyncSolicitation(Product.ProductId productId, BigDecimal price, Integer quantity) {
            this.productId = productId;
            this.price = price;
            this.quantity = quantity;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public Product.ProductId getProductId() {
            return productId;
        }

        public Integer getQuantity() {
            return quantity;
        }
    }
}
