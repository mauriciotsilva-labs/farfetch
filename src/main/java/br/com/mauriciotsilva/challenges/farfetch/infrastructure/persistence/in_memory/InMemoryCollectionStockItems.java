package br.com.mauriciotsilva.challenges.farfetch.infrastructure.persistence.in_memory;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.StockItem;
import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock.StockItems;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Repository
public class InMemoryCollectionStockItems implements StockItems {

    private final Map<Product.ProductId, StockItem> items = new HashMap<>();

    public StockItem update(Product product, Integer quantity) {
        return items.compute(product.getId(), (k, v) -> new StockItem(k, v == null ? quantity : v.getQuantity() + quantity));
    }

    public Optional<StockItem> byProductId(Product.ProductId productId) {
        return ofNullable(items.get(productId));
    }

}
