package br.com.mauriciotsilva.challenges.farfetch.domain.model.commission;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.commission.Boutique.BoutiqueId;

import java.util.Optional;

public interface Boutiques {

    void add(Boutique boutique);

    Optional<Boutique> byId(BoutiqueId id);
}
