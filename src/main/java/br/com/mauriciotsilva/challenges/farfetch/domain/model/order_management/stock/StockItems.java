package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;

import java.util.Optional;

public interface StockItems {

    StockItem update(Product product, Integer quantity);

    Optional<StockItem> byProductId(Product.ProductId productId);
}
