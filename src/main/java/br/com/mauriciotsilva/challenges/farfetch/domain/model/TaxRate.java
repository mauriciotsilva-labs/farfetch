package br.com.mauriciotsilva.challenges.farfetch.domain.model;

import java.math.BigDecimal;
import java.util.function.Function;

public interface TaxRate extends Function<BigDecimal, BigDecimal> {
}
