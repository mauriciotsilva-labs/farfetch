package br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.stock;

import br.com.mauriciotsilva.challenges.farfetch.domain.model.order_management.Product;

public class StockItem {

    private final Product.ProductId productId;
    private final Integer quantity;

    public StockItem(Product.ProductId productId, Integer quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public boolean isQuantityAvailable(int quantityToCheck) {
        return quantity - quantityToCheck >= 0;
    }

    public Product.ProductId getProductId() {
        return productId;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
