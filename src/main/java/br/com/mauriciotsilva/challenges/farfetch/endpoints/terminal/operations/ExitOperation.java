package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations;

import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.LisbonOperation;
import br.com.mauriciotsilva.challenges.farfetch.infrastructure.lisbon.Operation;

import java.io.IOException;

@LisbonOperation(alias = {"exit", "quit"})
public class ExitOperation implements Operation {

    @Override
    public void onExecute(String... params) throws IOException {
        System.exit(0);
    }
}
