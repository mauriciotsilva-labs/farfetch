package br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.io;

import br.com.mauriciotsilva.challenges.farfetch.application.order_management.CatalogApplicationService.CatalogSyncSolicitation;
import br.com.mauriciotsilva.challenges.farfetch.endpoints.terminal.operations.CsvCatalogLineToSyncSolicitation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static java.nio.file.Files.readAllLines;
import static java.util.stream.Collectors.toList;

@Component
public class CatalogSyncSolicitationCsvFileReaderAdapter {

    private final Logger logger = LoggerFactory.getLogger(CatalogSyncSolicitationCsvFileReaderAdapter.class);

    private final CsvCatalogLineToSyncSolicitation translator;

    @Autowired
    public CatalogSyncSolicitationCsvFileReaderAdapter(CsvCatalogLineToSyncSolicitation translator) {
        this.translator = translator;
    }

    public List<CatalogSyncSolicitation> adapt(Path path) throws IOException {

        logger.info("File '{}' going to be process", path);

        var lines = readAllLines(path)
                .stream()
                .peek(line -> logger.trace("Reading line '{}'", line));

        return translator.each(lines, toList());
    }
}
